# PARBATCH
parbatch is a way to run many small sequential jobs in parallel

``` shell
parbatch [-n <int>] joblist.txt
```

## Usage

parbatch is implemented as a mpirun wrapper which takes a file with a list of commands and executes them as
sequential jobs on a cluster system in parallel

parbatch can be used in several ways:

### sbatch usage

``` shell
parbatch joblist.txt
```

parbatch will automatically extract the correct number of nodes and processes per nodes from slurm and execute the jobs accordingly

An example job script can be found in the docs/example directory

### Interactive usage

``` shell
parbatch -n <int> joblist.txt
```

If parbatch is run interactively, one has to provide the number of tasks to use

### Format of the joblist file

``` shell
command 1a; command 1b; command 1c
command 2a; command 2b
...
command Na; command Nb; command Nc; command Nd
```

parbatch will extract N lines from the joblist file.
Each line is treated as one command and executed in a single task

## Example

An example sbatch script and joblist can be found in the parbatch  documentation directory

## Installation

Installation is straight-forward.

``` shell
# bash autogen.sh
# ./configure --prefix=/software/ MODULEPATH=/software/modulefiles
# make
# make install
```

The MODULEPATH variable can be used to put the parbatch lmod module into a custom directory.
If no MODULEPATH is specified `/usr/share/lmod/lmod/modulefiles` will be used
