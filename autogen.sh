#!/bin/bash
make distclean
rm -rf Makefile.in Makefile aclocal.m4 autom4te.cache build compile config.status configure~ configure config.log depcomp .deps dist install-sh missing 1.0.lua parbatch
mkdir build
aclocal
autoconf
automake --add-missing
