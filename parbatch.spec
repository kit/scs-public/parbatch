Name: parbatch
Version: 1.0
Release: %{?release}%{!?release:1}
Source0: %{name}.tar.bz2
License: GPL
Group: Applications/Tools
Packager: Sven Trautmann <sven.trautmann@kit.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: run serial jobs inside a parallel program
URL: https://gitlab.kit.edu/kit/scc/scs/parbatch

Requires: openmpi

# Turn off creation of debuginfo packages on RHEL
%if 0%{?fedora} || 0%{?rhel} || 0%{?centos}
%global debug_package %{nil}
%endif

%define category system
%define module_dir   /software/all/lmod/modulefiles/Core

%description
wrapper to run multiple sequential tasks in parallel using MPI

%prep
%setup -q -n parbatch

%build
# force redhat do to the right(tm) thing
%configure CFLAGS="-O2 -march=native -fPIC" CC=mpicc MODULEPATH=%{module_dir}/%{category} --prefix=%{_prefix} 
%{__make}

%install
%make_install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/*
%{_docdir}/*
%{module_dir}/%{category}/*

