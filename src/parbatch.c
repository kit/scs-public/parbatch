/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*

usage:

   parbatch joblist

   parbatch joblist1 joblist2 joblist3 ...

   joblist must be a file - it contains one command per line, every line
           is called with system(3), i.e. it can be something like
           cd foo  ; echo $BAR | sort > baz
*/


#define _GNU_SOURCE
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <mpi.h>

#include  <pthread.h>

enum { /* tags for master/slave communication */
  tag_work_request = 23,
  tag_work_assignement = 24,
  tag_abort = 25
};

int debug = 1;   /* debuglevel 0..2 */

int rank, np; /* rank, number of processes */

int provided_thread_support; /* what thread support does the MPI implementation
                                provide */
char *name_of_executable;    /* name of executable needed for error messages */

double t0;                   /* start time of application needed for debug
                                time output */

char **joblist;                 /* list of jobs to do                */
int max_number_jobs = 2;      /* current size of joblist           */
int number_jobs;                /* number jobs in the list           */
long int max_jobstring_length;  /* maximum length of one job string
                                   (needed for communication buffer
                                   allocation)                       */


void init_joblist(void)
{
  joblist = (char**)malloc(max_number_jobs*sizeof(char*));
  for( int i = 0; i < max_number_jobs; i++) joblist[i] = NULL;
  number_jobs = 0;
  max_jobstring_length = 0;
}

/*===============================================================================================*/
void read_joblist(char *filename)
{
  FILE *inputfile;

  if( filename == NULL )
    inputfile = stdin;
  else {
    inputfile = fopen(filename, "r");
    if( inputfile == NULL ) {
      fprintf(stderr, "%s: error: couldn't open file '%s'\n", name_of_executable, filename);
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
  }

  size_t n;
  while( getline(&(joblist[number_jobs]), &n, inputfile) != -1 ) {
    if( n > max_jobstring_length ) max_jobstring_length = n;
    number_jobs++;
    if( number_jobs == max_number_jobs ) {
      max_number_jobs *= 2;
      joblist = realloc(joblist, max_number_jobs*sizeof(char*));
      for( int j = number_jobs; j < max_number_jobs; j++) joblist[j] = NULL;
    }
  }

  if( debug >= 2 ) {
    for( int i = 0; i < number_jobs; i++)
      printf("# proc %d: joblist[%d]: '%s'\n", rank, i, joblist[i]);
  }
}

/*===============================================================================================*/
void *slave(void* not_used)
{
  double t1, t2;
  MPI_Status status;
  char *job;

  job = (char*)malloc(max_jobstring_length*sizeof(char));

  while( 1 ) {
    MPI_Send(NULL, 0, MPI_INT, 0, tag_work_request, MPI_COMM_WORLD);
    MPI_Recv(job, max_jobstring_length, MPI_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    if( status.MPI_TAG == tag_abort ) {
      if( debug >= 1 ) printf("# proc %d: No more jobs at time %f\n", rank, MPI_Wtime() - t0);
      break;
    }
    if( debug >= 1 ) {
      t1 = MPI_Wtime();
      printf("# proc %d: t=%f starting job\n", rank, t1-t0);
    }
    int retval = system(job);
    if( debug >= 1 ) {
      t2 = MPI_Wtime();
      printf("# proc %d: t=%f finished job (%f sec.) with retval=%d\n", rank, t2-t0, t2-t1, retval);
    }
  }

  free(job);
  return NULL;
}

/*===============================================================================================*/
int main(int argc, char **argv)
{
  name_of_executable = argv[0];

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided_thread_support);

  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if( rank == 0 && provided_thread_support != MPI_THREAD_MULTIPLE ) {
    fprintf(stderr, "%s: warning: your MPI environment allows only the ", name_of_executable);
    switch( provided_thread_support ) {
    case MPI_THREAD_SINGLE: fprintf(stderr, "MPI_THREAD_SINGLE"); break;
    case MPI_THREAD_FUNNELED: fprintf(stderr, "MPI_THREAD_FUNNELED"); break;
    case MPI_THREAD_SERIALIZED: fprintf(stderr, "MPI_THREAD_SERIALIZED"); break;
    case MPI_THREAD_MULTIPLE: fprintf(stderr, "MPI_THREAD_MULTIPLE"); break;
    default: fprintf(stderr, "%d", provided_thread_support);
    }
    fprintf(stderr, " multi-threading model; the master processor will not do any work\n");
  }

  if( provided_thread_support != MPI_THREAD_MULTIPLE && np == 1 ) {
    fprintf(stderr, "%s: error: there are no slave processes and the master won't do any work\n", name_of_executable);
    MPI_Abort(MPI_COMM_WORLD, 2);
  }

  t0 = MPI_Wtime();

  init_joblist();
  if( rank == 0 ) {
    if( argc == 1 )
      read_joblist(NULL);
    else
      for( int i = 1; i < argc; i++) read_joblist(argv[i]);
    if( debug ) {
      printf("# proc %d: number_jobs=%d\n", rank, number_jobs);
      printf("# proc %d: max_jobstring_length=%ld\n", rank, max_jobstring_length);
    }
  }
  MPI_Bcast(&max_jobstring_length, 1, MPI_LONG, 0, MPI_COMM_WORLD);

  MPI_Status status;

  if( rank == 0 ) {
    pthread_t child;
    int number_active_slaves = np-1;

    if( provided_thread_support == MPI_THREAD_MULTIPLE ) {
      pthread_create(&child, NULL, slave, NULL);
      number_active_slaves++;
    }

    int job_index;

    for (job_index = 0; job_index < number_jobs; job_index++)
    {
      MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      MPI_Send(joblist[job_index], max_jobstring_length, MPI_CHAR, status.MPI_SOURCE,
                   tag_work_assignement, MPI_COMM_WORLD);
    }

    for ( ;number_active_slaves != 0; number_active_slaves--)
    {
      MPI_Recv(NULL, 0, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      MPI_Send(NULL, 0, MPI_CHAR, status.MPI_SOURCE, tag_abort, MPI_COMM_WORLD);
    }
  }
  else
    slave(NULL);

  if( debug >= 1 ) {
    if( rank == 0 )
      printf("# proc %d: MASTER finished at time %f\n", rank, MPI_Wtime() - t0);
    else
      printf("# proc %d: SLAVE finished at time %f\n", rank, MPI_Wtime() - t0);
  }

  MPI_Finalize();
}
